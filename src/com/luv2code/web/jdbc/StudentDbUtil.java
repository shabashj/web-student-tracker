package com.luv2code.web.jdbc;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import com.mysql.jdbc.PreparedStatement;

//import com.mysql.jdbc.Connection;
import java.sql.Connection;

public class StudentDbUtil {
	private DataSource dataSource;

	public StudentDbUtil(DataSource theDataSource) {
		dataSource = theDataSource;
	}

	public List<Student> getStudents() throws Exception {
		List<Student> students = new ArrayList<>();

		Connection myConn = null;
		Statement myStmt = null;
		ResultSet myRs = null;

		try {
			// get a connection
			myConn = (Connection) dataSource.getConnection();

			// create SQL statement
			String sql = "select * from student";
			myStmt = myConn.createStatement();

			// execute query
			myRs = myStmt.executeQuery(sql);

			// process results
			while (myRs.next()) {

				int id = myRs.getInt("id");
				String firstName = myRs.getString("first_name");
				String lastName = myRs.getString("last_name");
				String email = myRs.getString("email");

				Student tempStudent = new Student(id, firstName, lastName, email);
				students.add(tempStudent);
			}

			return students;

		} finally {
			close(myConn, myStmt, myRs);
		}

	}

	private void close(Connection myConn, Statement myStmt, ResultSet myRs) {
		// TODO Auto-generated method stub

		try {
			if (myRs != null) {
				myRs.close();
			}

			if (myStmt != null) {
				myStmt.close();
			}
			if (myConn != null) {
				myConn.close();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void addStudent(Student theStudent) throws Exception {
		Connection myConn = null;
		java.sql.PreparedStatement myStmt = null;

		try {
			// get a connection
			myConn = (Connection) dataSource.getConnection();

			// create SQL statement
			String sql = "insert into student " + "(first_name, last_name, email) " + "values (?, ? ,?)";

			myStmt = myConn.prepareStatement(sql);

			myStmt.setString(1, theStudent.getFirstName());
			myStmt.setString(2, theStudent.getLastName());
			myStmt.setString(3, theStudent.getEmail());

			// execute query
			myStmt.execute();

		} finally {
			close(myConn, myStmt, null);
		}
	}

	public Student getStudent(String theStudentId) throws Exception {
		Student theStudent = null;

		Connection myConn = null;
		java.sql.PreparedStatement myStmt = null;
		ResultSet myRs = null;
		int studentId;

		try {
			// parse id
			studentId = Integer.parseInt(theStudentId);
			// connection to db
			myConn = dataSource.getConnection();
			// sql stmt
			String sql = "select * from student where id=?";

			myStmt = myConn.prepareStatement(sql);
			// set params
			myStmt.setInt(1, studentId);

			// execute statement
			myRs = myStmt.executeQuery();

			if (myRs.next()) {
				String firstName = myRs.getString("first_name");
				String lastName = myRs.getString("last_name");
				String email = myRs.getString("email");

				theStudent = new Student(studentId, firstName, lastName, email);
			} else {
				throw new Exception("Could not find student id - " + studentId);
			}

			return theStudent;

		} finally {
			// TODO: handle finally clause
			close(myConn, myStmt, myRs);
		}
	}

	public void updateStudent(Student theStudent) throws Exception {

		Connection myConn = null;
		java.sql.PreparedStatement myStmt = null;

		try {
			// get db connection
			myConn = dataSource.getConnection();

			// create sql update statement
			String sql = "update student set first_name=?, last_name=?, email=? where id=?";

			// prepare statement
			myStmt = myConn.prepareStatement(sql);

			// set params
			myStmt.setString(1, theStudent.getFirstName());
			myStmt.setString(2, theStudent.getLastName());
			myStmt.setString(3, theStudent.getEmail());
			myStmt.setInt(4, theStudent.getId());

			// execute sql statement
			myStmt.execute();
		} finally {
			close(myConn, myStmt, null);
		}

	}

	public void deleteStudent(String theStudentId) throws Exception {
		Connection myConn = null;
		java.sql.PreparedStatement myStmt = null;

		try {
			// parse id
			int studentId = Integer.parseInt(theStudentId);
			
			// get db connection
			myConn = dataSource.getConnection();

			// create sql update statement
			String sql = "delete from student where id=?";
			
			// prepare statement
			myStmt = myConn.prepareStatement(sql);
			
			myStmt.setInt(1, studentId);
			
			myStmt.execute();
		} finally {
			close(myConn, myStmt, null);
		}
	}
}
